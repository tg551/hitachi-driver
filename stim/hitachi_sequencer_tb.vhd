LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

use work.instruction_pkg.all;

ENTITY hitachi_sequencer_tb IS
END hitachi_sequencer_tb;
 
ARCHITECTURE behavior OF hitachi_sequencer_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
  component hitachi_sequencer is
    generic (
      four_bit_mode: boolean
      );
    port (
      clk              : in    std_logic;
      reset            : in    std_logic;
      enable           : in    std_logic;
      instr_in         : in    instruction;
      ignore_busy_flag : in    std_logic;
      busy             : out   std_logic;
      instr_out        : inout std_logic_vector(9 downto 0);
      enable_out       : out   std_logic
    );
  end component;

  signal clk              : std_logic;
  signal reset            : std_logic;
  signal enable           : std_logic;
  signal instr_in         : instruction;
  signal ignore_busy_flag : std_logic;
  signal busy             : std_logic;
  signal instr_out        : std_logic_vector(9 downto 0);
  signal enable_out       : std_logic;

  -- Clock period definitions
  constant clk_period : time := 10 ns;


  -- internal tb signals
  signal unset_bf : std_logic;
 
begin
 
	-- Instantiate the Unit Under Test (UUT)
  uut: entity work.hitachi_sequencer
    generic map(
      four_bit_mode => true
      )
    port map (
      clk              => clk,
      reset            => reset,
      enable           => enable,
      instr_in         => instr_in,
      ignore_busy_flag => ignore_busy_flag,
      busy             => busy,
      instr_out        => instr_out,
      enable_out       => enable_out
    );

   -- Clock process definitions
   clk_process :process
   begin
     clk <= '0';
     wait for clk_period/2;
     clk <= '1';
     wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
     unset_bf <= '0';
     
      -- hold reset state for 100 ns.
     reset <= '1';
     wait for 100 ns;	
     reset <= '0';
     wait for clk_period*10;
     
     -- Set an instruction and start the low level sequencer
     instr_in <= set_cgram_addr("111000");
     ignore_busy_flag <= '1';
     wait until rising_edge(clk);
     enable <= '1';
     wait until rising_edge(clk);
     enable <= '0';

     wait for clk_period * 5;

     -- Make sure that data that should be latched is not changed.
     instr_in <= set_cgram_addr("000111");
     ignore_busy_flag <= '0';

     --Send another instruction
     wait for 10 us;
     wait until rising_edge(clk);
     enable <=  '1';
     wait until rising_edge(clk);
     enable <= '0';

     --wait a bit, to force the low level sequencer to spin, then unset the
     --busy flag
     wait for 20 us;
     unset_bf <= '1';
     wait for 30 us;
     unset_bf <= '0';
     wait;
   end process;

--instr_out <= (others => 'Z');
  instr_out <= "ZZ01110000" when instr_out(9 downto 8) = "01" and unset_bf = '1'
               else "ZZ10000000" when instr_out(9 downto 8) = "01"
               else (others => 'Z');
end;


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY init_tb IS
END init_tb;
 
ARCHITECTURE behavior OF init_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT hitachi_driver
    PORT(
         clk     : IN  std_logic;
         reset_b : IN  std_logic;
         test    : OUT  std_logic_vector(9 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset_b : std_logic := '0';

 	--Outputs
   signal test : std_logic_vector(9 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: hitachi_driver PORT MAP (
          clk => clk,
          reset_b => reset_b,
          test => test
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      reset_b <= '0';
		wait for 100 ns;	
      reset_b <= '1';
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

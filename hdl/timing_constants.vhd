library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Time constants necessary to achieve correct timings to operate an HD44780 style LCD display.
--
-- Note: There different individual modules will have different timing characteristics. You may
-- need to change some of these constants to suit your own device. These timings are based off a
-- LCM1602C LCD display
--
-- Note: The only datasheet that I could find is in chinese, and the timing diagram 
-- in the datasheet does not match the timing constants stated elsewhere in the sheet.
-- These constants may not actually produce correct driving behaviour.
--
-- The timing sequence that these constants are able to implement is as follows:
--   1) Set the RS, RW, and data pins
--   2) Delay for the longest pre-rising-edge-of-enable delay in the datasheet (tsu1 in the
--      diagram, possible tdsu1 in the table)
--   3) Set the enable flag
--   4) Delay for the minimum enable-high width.
--   5) Unset the enable flag
--   6) Delay for the minimum enable-low width.
--
--   note: This sequence is a little pessimistic, because we are allowed set RS, RW and data
--   during the enable low period, but we will not get any useful speedup by doing so.
--
--   additional note: The datasheet specifies an minimum total cycle time (tc, 1000ns). 
--   We should be able to meet this without any explicit effort.
--   
package timing_constants is
  
  -- To give a little extra timing confidence, we will slightly lengthen each datasheet value.
  constant slowdown_multiplier: real := 1.5;

  constant data_setup_time  : time := slowdown_multiplier * 200 ns;
  constant enable_high_time : time := slowdown_multiplier * 450 ns;
  constant enable_low_time  : time := slowdown_multiplier * 450 ns;
end package;
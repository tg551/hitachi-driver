library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- This package allows Hitachi HD44780 instructions to be built by calling functions.
-- Compared to building instructions by specifying bitstrings, this should make the
-- code more readable because this package attaches useful, meaningful names to the
-- instructions.
--
-- This webpage was used to source the information encoded in this package:
-- http://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller

-- TODO: Decide whether boolean would be more appropriate than std_logic for these
--       functions.... I get the feeling it depends on whether these instruction
--       functions will get called in a static manner... If the arguments are always
--       fixed, then boolean is more appropriate. If the arguments need to be driven
--       by signals, then std_logic would be more appropriate, otherwise we would
--       have to define a (otherwise pointless) conversion function from boolean to
--       std_logic

package instruction_pkg is

  -- This is a vector of the 10 bits that are sent to the HD44780 in an
  -- instruction. The format is as follows:
  -- Bit 9:          Register Select
  -- Bit 8:          Read/write flag
  -- Bit 7 downto 0: Data bits
  subtype instruction is std_logic_vector(9 downto 0);

  function empty_instr                                               return instruction;
  function clear_display                                             return instruction;
  function cursor_home                                               return instruction;
  function entry_mode_set(inc_dec : std_logic; shift : std_logic)    return instruction;
  function display_onoff_ctrl(
    display_onoff : std_logic; 
    cursor_onoff  : std_logic;
    blink_onoff   : std_logic
  )                                                                  return instruction;
  
  function display_shift(left_right : std_logic)                     return instruction;
  function cursor_shift(left_right : std_logic)                      return instruction;
  function set_cgram_addr(addr : std_logic_vector(5 downto 0))       return instruction;
  function set_ddram_addr(addr : std_logic_vector(6 downto 0))       return instruction;
  function read_bf_or_address                                        return instruction;
  function write_ram_data(write_data : std_logic_vector(7 downto 0)) return instruction;
end package;

package body instruction_pkg is
  function empty_instr return instruction is
  begin
    return "0000000000";
  end function;       
  
  function clear_display return instruction is
  begin
    return "0000000001";
  end function;
  
  function cursor_home return instruction is
  begin
    return "0000000010";
  end function;
  
  function entry_mode_set(inc_dec : std_logic; shift : std_logic)
  return instruction is
  begin
    return "00000001" & inc_dec & shift;
  end function;
  
  function display_onoff_ctrl(
    display_onoff : std_logic; 
    cursor_onoff  : std_logic;
    blink_onoff   : std_logic
  ) return instruction is 
  begin
    return "0000001" & display_onoff & cursor_onoff & blink_onoff;
  end function;
  
  function display_shift(left_right : std_logic) return instruction is
  begin
    return "0000011" & left_right & "00";
  end function;
  
  function cursor_shift(left_right : std_logic) return instruction is
  begin
    return "0000010" & left_right & "00";
  end function;
  
  function set_cgram_addr(addr : std_logic_vector(5 downto 0))
  return instruction is
  begin
    return "0001" & addr;
  end function;
  
  function set_ddram_addr(addr : std_logic_vector(6 downto 0))
  return instruction is
  begin
    return "001" & addr;
  end function;
  
  function read_bf_or_address return instruction is 
  begin 
    return "01ZZZZZZZZ";
  end function;
  
  function write_ram_data(write_data : std_logic_vector(7 downto 0)) return instruction is
  begin
    return "10" & write_data;
  end function;
  
  function read_ram return instruction is
  begin
    return "11ZZZZZZZZ";
  end function;
  
end package body;

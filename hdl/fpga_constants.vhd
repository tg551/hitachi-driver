library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- FPGA-level constants
package fpga_constants is

  -- The clock frequency of the FPGA. Used for deriving timers.
  constant fpga_clock_freq: natural := 100_000_000;
  --constant fpga_clock_freq: natural := 100_000;
end package;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.instruction_pkg.ALL;
use WORK.fpga_constants.ALL;

entity hitachi_driver is
  port(
    clk        : in  std_logic;
    reset_b    : in  std_logic;
    enable_out : out std_logic;
    instr_out  : out std_logic_vector(9 downto 0);
    test       : out std_logic_vector(9 downto 0)
  );
end hitachi_driver;

architecture rtl of hitachi_driver is
  signal reset : std_logic;

  -- The state machine described by hitachi_state is able to initialise an HD44780 
  -- The ten-step initialzation sequence has been liften from:
  -- http://web.alfredstate.edu/weimandn/lcd/lcd_initialization/lcd_initialization_index.html

  -- State machine
  subtype hitachi_state is std_logic_vector(18 downto 0);
  signal state                       : hitachi_state;
  constant init_wait_electric_stable : hitachi_state := (0  => '1', others => '0');  --step 1
  constant init_func_set_1           : hitachi_state := (1  => '1', others => '0');  --step 2
  constant init_wait_5ms             : hitachi_state := (2  => '1', others => '0');
  constant init_func_set_2           : hitachi_state := (3  => '1', others => '0');  --step 3
  constant init_wait_100us_1         : hitachi_state := (4  => '1', others => '0');
  constant init_func_set_3           : hitachi_state := (5  => '1', others => '0');  --step 4
  constant init_wait_100us_2         : hitachi_state := (6  => '1', others => '0');
  constant init_func_set_4bit_mode_1 : hitachi_state := (7  => '1', others => '0');  --step 5
  constant init_wait_100us_3         : hitachi_state := (8  => '1', others => '0');
  constant init_func_set_4bit_mode_2 : hitachi_state := (9  => '1', others => '0');  --step 6 (we can enable standard delay or start reading the busy flag here)
  constant init_disable_enable_1     : hitachi_state := (10 => '1', others => '0');
  constant init_disp_ctrl_all_off    : hitachi_state := (11 => '1', others => '0');  --step 7
  constant init_disable_enable_2     : hitachi_state := (12 => '1', others => '0');
  constant init_clear_display        : hitachi_state := (13 => '1', others => '0');  --step 8
  constant init_disable_enable_3     : hitachi_state := (14 => '1', others => '0');
  constant init_entry_mode_set       : hitachi_state := (15 => '1', others => '0');  --step 9
  constant init_disable_enable_4     : hitachi_state := (16 => '1', others => '0');
  constant init_enable_blink         : hitachi_state := (17 => '1', others => '0');  --step 10
  constant init_idle                 : hitachi_state := (18 => '1', others => '0');

  -- Communication with low level sequencer
  signal enable        : std_logic;
  signal enable_d1     : std_logic;
  signal enable_strobe : std_logic;

  signal ignore_busy_flag : std_logic;
  signal busy             : std_logic;

  -- timing constants
  constant hundred_ms_delay_count : natural := fpga_clock_Freq / (1.0 sec / 100 ms);
  constant five_ms_delay_count    : natural := fpga_clock_freq / (1.0 sec / 5 ms);
  constant hundred_us_delay_count : natural := fpga_clock_freq / (1.0 sec / 100 us);2

  -- delay counter signals
  signal run_counter             : std_logic;
  signal counter                 : natural range 0 to hundred_ms_delay_count;
  signal counter_max             : natural range 0 to hundred_ms_delay_count;
  signal counter_finished        : std_logic;
  signal counter_finished_d1     : std_logic;
  signal counter_finished_strobe : std_logic;

begin
  reset <= not reset_b;

  sequencer: entity WORK.hitachi_sequencer
  port map(
    clk              => clk,
    reset            => reset,
    enable           => enable_strobe,
    instr_in         => instr_in,
    ignore_busy_flag => ignore_busy_flag,

    busy             => busy,

    instr_out  => instr_out,
    enable_out => enable_out,
  );
  
  counting: process(clk, reset)
  begin
    if reset = '1' then
      counter_finished <= '0';
      counter <= 0;
    elsif rising_edge(clk) then
      counter_finished <= '0';
      
      if run_counter = '1' then
        if counter < counter_max then
          counter <= counter + 1;
        else
          counter_finished <= '1';
        end if;
      else
        counter <= 0;
      end if;
    end if;
  end process;
  
  counter_strobe: process(clk, reset)
  begin
    if reset = '1' then
      counter_finished_d1 <= '0';
    elsif rising_edge(clk) then
      counter_finished_d1 <= counter_finished;
    end if;
  end process;
  
  counter_finished_strobe <= '1' when counter_finished = '1' and counter_finished_d1 = '0' 
                                 else '0';

  fsm_nextstate: process(clk, reset)
  begin
    if reset = '1' then
      state <= (others => '0');
    elsif rising_edge(clk) then
      case state is
        when init_wait_electric_stable  =>
          if counter_finished = '1' then
            state <= init_func_set_1;
          end if;
          
        when init_func_set_1  =>
          if busy = '1' then
            state <= init_wait_5ms;
          end if;
          
        when init_wait_5ms  =>
          if counter_finished = '1' then
            state <= init_func_set_2;
          end if;
        
        when init_func_set_2  =>
          if busy = '1' then
            state <= init_wait_100us_1;
          end if;
        
        when init_wait_100us_1  =>
          if counter_finished = '1' then
            state <= init_func_set_3;
          end if;
        
        when init_func_set_3  =>
           if busy = '1' then
            state <= init_wait_100us_2;
          end if;
        
        when init_wait_100us_2  =>
          if counter_finished = '1' then
            state <= init_func_set_4bit_mode_1;
          end if;
        
        when init_func_set_4bit_mode_1  =>
          if busy = '1' then
            state <= init_wait_100us_3;
          end if;
        
        when init_wait_100us_3  =>
          if counter_finished = '1' then
            state <= init_func_set_4bit_mode_2;
          end if;
          
        when init_func_set_4bit_mode_2  =>
          if busy = '1' then
            state <= init_disable_enable_1;
          end if;
       
        when init_disable_enable_1 =>
          state <= init_disp_ctrl_all_off;
          
        when init_disp_ctrl_all_off  =>
          if busy = '1' then
            state <= init_disable_enable_2;
          end if;
       
        when init_disable_enable_2 =>
          state <= init_clear_display;
       
        when init_clear_display  =>
          if busy = '1' then
            state <= init_disable_enable_3;
          end if;
       
        when init_disable_enable_3 => 
          state <= init_entry_mode_set;
        
        when init_entry_mode_set  =>
          if busy = '1' then
            state <= init_disable_enable_4;
          end if;
       
        when init_disable_enable_4 =>
          state <= init_enable_blink;
        
        when init_enable_blink  =>
          if busy = '1' then
            state <= init_idle;
          end if;
        
        when init_idle =>
          null;
        
        when others =>
          state <= init_wait_electric_stable;
      end case;
    end if;
  end process;
  
  
  fsm_outputs: process(clk, reset)
  begin
    if reset = '1' then
      run_counter <= '0';
      enable <= '0';
      counter_max <= 0;
      ignore_busy_flag <= '0';
    elsif rising_edge(clk) then
      run_counter <= '0';
      enable <= '0';
      counter_max <= 0;
      ignore_busy_flag <= '0';
      
      case state is
        when init_wait_electric_stable =>
          run_counter <= '1';
          counter_max <= hundred_ms_delay_count;
         
        when init_func_set_1 =>
          enable <= '1';
          ignore_busy_flag <= '1';
         
        when init_wait_5ms =>
          run_counter <= '1';
          counter_max <= five_ms_delay_count;
         
        when init_func_set_2 =>
          enable <= '1';
          ignore_busy_flag <= '1';
         
        when init_wait_100us_1 =>
          run_counter <= '1';
          counter_max <= hundred_us_delay_count;
         
        when init_func_set_3 =>
          enable <= '1';
          ignore_busy_flag <= '1';
        
        when init_wait_100us_2 =>
          run_counter <= '1';
          counter_max <= hundred_us_delay_count;
        
        when init_func_set_4bit_mode_1 =>
          enable <= '1';
          ignore_busy_flag <= '1';
        
        when init_wait_100us_3 =>
          run_counter <= '1';
          counter_max <= hundred_us_delay_count;
        
        when init_func_set_4bit_mode_2 =>
          enable <= '1';
         
        when init_disable_enable_1 =>
          null;
         
        when init_disp_ctrl_all_off =>
          enable <= '1';
        
        when init_disable_enable_2 =>
          null;
        
        when init_clear_display =>
          enable <= '1';

        when init_disable_enable_3 =>
          null;
        
        when init_entry_mode_set =>
          enable <= '1';
         
        when init_disable_enable_4 =>
          null;
         
        when init_enable_blink =>
          enable <= '1';
         
        when init_idle =>
          null;
         
        when others =>
          null;
      end case;
    end if;
  end process;
  
  en_strobe: process(clk, reset)
  begin
    if reset = '1' then
      enable_d1 <= '0';
    elsif rising_edge(clk) then
      enable_d1 <= enable;
    end if;
  end process;
  
  enable_strobe <= '1' when enable = '1' and enable_d1 = '0' 
                       else '0';
  
end rtl;

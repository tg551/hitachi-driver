library ieee;
use ieee.std_logic_1164.all;
use work.instruction_pkg.all;
use work.fpga_constants.all;
use work.timing_constants.all;

entity hitachi_sequencer is
  generic(
    four_bit_mode : boolean
  );
  port(
    clk              : in std_logic;
    reset            : in std_logic;
    enable           : in std_logic;    -- Enable strobe!
    instr_in         : in instruction;
    ignore_busy_flag : in std_logic;

    busy : out std_logic;

    instr_out  : inout std_logic_vector(9 downto 0);  -- Outputs directly to the device
    enable_out : out   std_logic
  );
end hitachi_sequencer;

architecture rtl of hitachi_sequencer is

  --state machine
  subtype fsm_state is std_logic_vector(9 downto 0);
  signal state                       : fsm_state;
  constant fsm_ready                 : fsm_state := (0 => '1', others => '0');
  constant fsm_latch_inputs          : fsm_state := (1 => '1', others => '0');
  constant fsm_delay_setup           : fsm_state := (2 => '1', others => '0');
  constant fsm_reset_counter_1       : fsm_state := (3 => '1', others => '0');
  constant fsm_enable_high           : fsm_state := (4 => '1', others => '0');
  constant fsm_reset_counter_2       : fsm_state := (5 => '1', others => '0');
  constant fsm_enable_low            : fsm_state := (6 => '1', others => '0');
  constant fsm_latch_read_bf_input   : fsm_state := (7 => '1', others => '0');
  constant fsm_process_busy_result   : fsm_state := (8 => '1', others => '0');
  constant fsm_dispatch_after_en_low : fsm_state := (9 => '1', others => '0');

  --delay constants
  constant data_setup_delay_count  : natural := fpga_clock_freq / (1.0 sec / data_setup_time);
  constant enable_high_delay_count : natural := fpga_clock_freq / (1.0 sec / enable_high_time);
  constant enable_low_delay_count  : natural := fpga_clock_freq / (1.0 sec / enable_low_time);

  --delay counter
  signal counter_finished        : std_logic;
  signal counter_finished_d1     : std_logic;
  signal counter_finished_strobe : std_logic;
  signal run_counter             : std_logic;
  signal counter                 : natural range 0 to enable_low_delay_count;
  signal counter_max             : natural range 0 to enable_low_delay_count;

  -- latched inputs
  signal latch_instr_in         : instruction;
  signal latch_ignore_busy_flag : std_logic;

  --???
  signal top_half                 : std_logic;
  signal current_instr_is_read_bf : std_logic;

begin
  
  fsm_nextstate: process(clk, reset)
  begin
    if reset = '1' then
      state <= (others => '0');
    elsif rising_edge(clk) then
      case state is
    
        when fsm_ready =>
          if enable = '1' then
            state <= fsm_latch_inputs;
          end if;
       
        when fsm_latch_inputs =>
          state <= fsm_delay_setup;
      
        when fsm_delay_setup =>
          if counter_finished_strobe = '1' then
            state <= fsm_reset_counter_1;
          end if;

        when fsm_reset_counter_1 =>
          state <= fsm_enable_high;
          
        when fsm_enable_high =>
          if counter_finished_strobe = '1' then
            state <= fsm_reset_counter_2;
          end if;

        when fsm_reset_counter_2 =>
          state <= fsm_enable_low;
          
        when fsm_enable_low =>
          if counter_finished_strobe = '1' then
            state <= fsm_dispatch_after_en_low;
          end if;

        when fsm_dispatch_after_en_low =>
          if four_bit_mode and top_half = '1' then
            state <= fsm_delay_setup;   -- This is a bit of a hack. Before
                                        -- sending the second half the the data
                                        -- byte, we must delay a little bit so
                                        -- that we do not violate minimum
                                        -- enable-to-enable strobe time. There
                                        -- is no explicit delay for this, so we
                                        -- enter the setup hold time delay state.
          else
            if latch_ignore_busy_flag = '1' then
              state <= fsm_ready;
            elsif current_instr_is_read_bf = '0' then
              state <= fsm_latch_read_bf_input;
            else
              state <= fsm_process_busy_result;
            end if;
          end if;
        
        when fsm_process_busy_result =>
          if instr_out(7) = '0' then
            state <= fsm_ready;
          else
            state <= fsm_latch_read_bf_input;
          end if;
          
        when fsm_latch_read_bf_input =>
          state <= fsm_delay_setup;
        
        when others =>
          state <= fsm_ready;

        end case;
    end if;
  end process;
  
  fsm_outputs: process(clk, reset)
  begin
    if reset = '1' then
      run_counter <= '0';
      enable_out  <= '0';
      counter_max <= 0;
      instr_out <= empty_instr;
      current_instr_is_read_bf <= '0';

      latch_ignore_busy_flag <= '0';
      latch_instr_in         <= empty_instr;
      
    elsif rising_edge(clk) then
      run_counter <= '0';
      enable_out  <= '0';
      counter_max <= 0;

      case state is

        when fsm_ready =>
          current_instr_is_read_bf <= '0';
          if four_bit_mode then
            top_half <= '1';
          end if;
          
        when fsm_latch_inputs =>
          latch_ignore_busy_flag <= ignore_busy_flag;
          latch_instr_in         <= instr_in;
      
        when fsm_delay_setup =>
          if four_bit_mode and top_half = '1' then
            -- Output rs and rw, and the top four bits of data. 
            instr_out(9 downto 4) <= latch_instr_in(9 downto 4);
          elsif four_bit_mode and top_half = '0' then
            -- Output rs and wr, and the bottom four bits of data.
            instr_out(9 downto 4) <= latch_instr_in(9 downto 8) & latch_instr_in(3 downto 0);
          else
            instr_out <= latch_instr_in;
          end if;
          run_counter <= '1';
          counter_max <= data_setup_delay_count;

        when fsm_reset_counter_1 =>
          null;
          
        when fsm_enable_high =>
          run_counter <= '1';
          counter_max <= enable_high_delay_count;
          enable_out  <= '1';

        when fsm_reset_counter_2 =>
          null;
          
        when fsm_enable_low =>
          run_counter <= '1';
          counter_max <= enable_low_delay_count;

        when fsm_dispatch_after_en_low =>
          if four_bit_mode and top_half = '1' then
            top_half <= '0';
          elsif current_instr_is_read_bf = '0' then
            top_half <= '1';
          end if;
          
        when fsm_process_busy_result =>
            top_half <= '1';
          
        when fsm_latch_read_bf_input =>
          latch_instr_in <= read_bf_or_address;
          current_instr_is_read_bf <= '1';
        
        when others =>
          null;

        end case;
    end if;
  end process;

  busy <= '0' when state = fsm_ready else '1';
  
  counting: process(clk, reset)
  begin
    if reset = '1' then
      counter_finished <= '0';
      counter          <= 0;
    elsif rising_edge(clk) then
      counter_finished <= '0';
      
      if run_counter = '1' then
        if counter < counter_max then
          counter <= counter + 1;
        else
          counter_finished <= '1';
        end if;
      else
        counter <= 0;
      end if;
    end if;
  end process;
  
counter_finish_delay: process (clk, reset) is
begin
  if reset = '1' then
    counter_finished_d1 <= '0';
  elsif rising_edge(clk) then
    counter_finished_d1 <= counter_finished;
  end if;
end process counter_finish_delay;
counter_finished_strobe <= '1' when counter_finished = '1' and counter_finished_d1 = '0'
                         else '0';
  
end architecture;

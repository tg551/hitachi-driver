#quietly set PROJECT_DIR "C:/Users/t/Desktop/xilinx/projects/hitachi-driver"
quietly set PROJECT_DIR "C:/Users/Thomas.Green/Desktop/hitachi-driver/hitachi-driver"

vlib presynth
vmap presynth presynth
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/hdl/instructions.vhd"
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/hdl/fpga_constants.vhd"
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/hdl/timing_constants.vhd"
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/hdl/hitachi_sequencer.vhd"
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/hdl/hitachi_driver.vhd"
vcom -93 -explicit  -work presynth "${PROJECT_DIR}/stim/init_tb.vhd"

vsim presynth.init_tb

radix define init_radix {
  19'b0000000000000000001 "init_wait_electric_stable",
  19'b0000000000000000010 "init_func_set_1",
  19'b0000000000000000100 "init_wait_5ms",
  19'b0000000000000001000 "init_func_set_2",
  19'b0000000000000010000 "init_wait_100us_1",
  19'b0000000000000100000 "init_func_set_3",
  19'b0000000000001000000 "init_wait_100us_2",
  19'b0000000000010000000 "init_func_set_4bit_mode_1",
  19'b0000000000100000000 "init_wait_100us_3",
  19'b0000000001000000000 "init_func_set_4bit_mode_2",
  19'b0000000010000000000 "init_disable_enable_1",
  19'b0000000100000000000 "init_disp_ctrl_all_off",
  19'b0000001000000000000 "init_disable_enable_2",
  19'b0000010000000000000 "init_clear_display",
  19'b0000100000000000000 "init_disable_enable_3",
  19'b0001000000000000000 "init_entry_mode_set",
  19'b0010000000000000000 "init_disable_enable_4",
  19'b0100000000000000000 "init_enable_blink",
  19'b1000000000000000000 "init_idle"
}

radix define seq_radix {
  8'b00000001 "fsm_ready",
  8'b00000010 "fsm_latch_inputs",
  8'b00000100 "fsm_delay_setup",
  8'b00001000 "fsm_enable_high",
  8'b00010000 "fsm_enable_low",
  8'b00100000 "fsm_latch_read_bf_input",
  8'b01000000 "fsm_process_busy_result",
  8'b10000000 "fsm_done"
}

add wave -label clk                                   /init_tb/clk
add wave -label reset                                 /init_tb/uut/reset
add wave -label init_state       -radix init_radix    /init_tb/uut/state
add wave -label counter          -radix unsigned      /init_tb/uut/counter
add wave -label count_fin_strob  -radix unsigned      /init_tb/uut/counter_finished_strobe
add wave -label seq_enable                            /init_tb/uut/enable
add wave -label seq_enable_strobe                     /init_tb/uut/enable_strobe
add wave -label seq_state        -radix seq_radix     /init_tb/uut/sequencer/state
add wave -label seq_counter      -radix unsigned      /init_tb/uut/sequencer/counter
add wave -label seq_counter_done                      /init_tb/uut/sequencer/counter_finished

run 50 us
wave zoom full
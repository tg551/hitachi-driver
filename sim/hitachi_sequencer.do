#quietly set PROJECT_DIR "C:/Users/t/Desktop/xilinx/projects/hitachi-driver"
quietly set PROJECT_DIR "C:/Users/Thomas.Green/Desktop/hitachi-driver/hitachi-driver"

vlib synth_hitachi_sequencer
vmap synth_hitachi_sequencer synth_hitachi_sequencer
vcom -93 -explicit  -work synth_hitachi_sequencer "${PROJECT_DIR}/hdl/instructions.vhd"
vcom -93 -explicit  -work synth_hitachi_sequencer "${PROJECT_DIR}/hdl/fpga_constants.vhd"
vcom -93 -explicit  -work synth_hitachi_sequencer "${PROJECT_DIR}/hdl/timing_constants.vhd"
vcom -93 -explicit  -work synth_hitachi_sequencer "${PROJECT_DIR}/hdl/hitachi_sequencer.vhd"
vcom -93 -explicit  -work synth_hitachi_sequencer "${PROJECT_DIR}/stim/hitachi_sequencer_tb.vhd"

vsim synth_hitachi_sequencer.hitachi_sequencer_tb

radix define seq_radix {
  10'b0000000001 "fsm_ready",
  10'b0000000010 "fsm_latch_inputs",
  10'b0000000100 "fsm_delay_setup",
  10'b0000001000 "fsm_reset_counter_1",
  10'b0000010000 "fsm_enable_high",
  10'b0000100000 "fsm_reset_counter_2"
  10'b0001000000 "fsm_enable_low",
  10'b0010000000 "fsm_latch_read_bf_input",
  10'b0100000000 "fsm_process_busy_result",
  10'b1000000000 "fsm_dispatch_after_en_low"
}

  
add wave             -label clk                          /hitachi_sequencer_tb/clk 
add wave             -label reset                        /hitachi_sequencer_tb/reset 
add wave -group port -label enable                       /hitachi_sequencer_tb/enable 
add wave -group port -label instr_in    -radix binary    /hitachi_sequencer_tb/instr_in 
add wave -group port -label ignore_bf                    /hitachi_sequencer_tb/ignore_busy_flag 
add wave -group port -label busy                         /hitachi_sequencer_tb/busy 
add wave -group port -label instr_out   -radix binary    /hitachi_sequencer_tb/instr_out 
add wave -group port -label en_out                       /hitachi_sequencer_tb/enable_out 

add wave             -label seq_counter -radix seq_radix /hitachi_sequencer_tb/uut/state      

add wave -group counter -label finished                    /hitachi_sequencer_tb/uut/counter_finished 
add wave -group counter -label fin_strob                   /hitachi_sequencer_tb/uut/counter_finished_strobe
add wave -group counter -label run                         /hitachi_sequencer_tb/uut/run_counter 
add wave -group counter -label counter     -radix unsigned /hitachi_sequencer_tb/uut/counter
add wave -group counter -label max         -radix unsigned /hitachi_sequencer_tb/uut/counter_max
add wave -group counter -label setup_delay -radix unsigned /hitachi_sequencer_tb/uut/data_setup_delay_count
add wave -group counter -label en_hi_delay -radix unsigned /hitachi_sequencer_tb/uut/enable_high_delay_count
add wave -group counter -label en_lo_delay -radix unsigned /hitachi_sequencer_tb/uut/enable_low_delay_count

add wave -group internal -label latch_bf                  /hitachi_sequencer_tb/uut/latch_ignore_busy_flag
add wave -group internal -label top_half                  /hitachi_sequencer_tb/uut/top_half
add wave -group internal -label unset_bf                  /hitachi_sequencer_tb/unset_bf
add wave -group internal -label latch_instr -radix binary /hitachi_sequencer_tb/uut/latch_instr_in
run 70 us
wave zoom full